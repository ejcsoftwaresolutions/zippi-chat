import invert from "invert-color";
import { Dimensions, I18nManager, Platform } from "react-native";
import { Appearance } from "react-native-appearance";

export const WINDOW_WIDTH = Dimensions.get("window").width;
export const WINDOW_HEIGHT = Dimensions.get("window").height;
const COLOR_SCHEME = Appearance.getColorScheme();

const lightColorSet = {
  mainThemeBackgroundColor: "#ffffff",
  mainTextColor: "#555555",
  mainSubtextColor: "#7e7e7e",
  mainThemeForegroundColor: "#1ce6e9",
  hairlineColor: "#e0e0e0",
  grey0: "#eaeaea",
  grey3: "#e6e6f2",
  grey6: "#d6d6d6",
  grey9: "#939393",
  whiteSmoke: "#f5f5f5",
  grey: "grey",
};

const darkColorSet = {
  mainThemeBackgroundColor: "#111111",
  mainTextColor: invert("#000"),
  mainSubtextColor: invert("#7e7e7e"),
  mainThemeForegroundColor: "#1ce6e9",
  hairlineColor: invert("#e0e0e0"),
  grey0: invert("#eaeaea"),
  grey3: invert("#e6e6f2"),
  grey6: invert("#d6d6d6"),
  grey9: invert("#939393"),
  whiteSmoke: invert("#f5f5f5"),
  grey: "grey",
};

const colorSet = {
  dark: darkColorSet,
  light: lightColorSet,
  "no-preference": lightColorSet,
  mainThemeBackgroundColor: "#ffffff",
  mainTextColor: "#555555",
  mainSubtextColor: "#7e7e7e",
  mainThemeForegroundColor: "#1ce6e9",
  hairlineColor: "#e0e0e0",
  grey0: "#eaeaea",
  grey3: "#e6e6f2",
  grey6: "#d6d6d6",
  grey9: "#939393",
  whiteSmoke: "#f5f5f5",
  backgroundScreenGrey: "#F6F6F6",
  grey: "grey",
};

const navLight = {
  backgroundColor: "#fff",
  fontColor: "#000",
  headerStyleColor: "#E8E8E8",
  iconBackground: "#F4F4F4",
  activeTintColor: "#0cccbb",
};

const navDark = {
  backgroundColor: invert("#fff"),
  fontColor: invert("#000"),
  headerStyleColor: invert("E8E8E8"),
  iconBackground: invert("#e6e6f2"),
  activeTintColor: "#0cccbb",
};

const navThemeConstants = {
  light: navLight,
  dark: navDark,
  "no-preference": navLight,
};

const fontFamily = {
  light: "Poppins-Light",
  extraLight: "Poppins-ExtraLight",
  main: "Poppins-Thin",
  medium: "Poppins-Medium",
  bold: "Poppins-SemiBold",
  black: "Poppins-Black",
  regular: "Poppins-Regular",
};

const iconSet = {};

const fontSet = {
  xxlarge: 40,
  xlarge: 30,
  large: 25,
  middle: 20,
  normal: 16,
  small: 13,
  xsmall: 11,
  title: 30,
  content: 20,
};

const sizeSet = {
  buttonWidth: "65%",
  inputWidth: "80%",
  radius: 25,
};

const styleSet = {
  menuBtn: {
    container: {
      backgroundColor: colorSet[COLOR_SCHEME].grayBgColor,
      borderRadius: 22.5,
      padding: 10,
      marginLeft: 10,
      marginRight: 10,
    },
    icon: {
      tintColor: "black",
      width: 16,
      height: 16,
    },
  },
  searchBar: {
    container: {
      marginLeft: Platform.OS === "ios" ? 30 : 0,
      backgroundColor: "transparent",
      borderBottomColor: "transparent",
      borderTopColor: "transparent",
      flex: 1,
    },
    input: {
      backgroundColor: colorSet[COLOR_SCHEME].inputBgColor,
      borderRadius: 10,
    },
  },
  rightNavButton: {
    marginRight: 10,
  },
  backArrowStyle: {
    resizeMode: "contain",
    tintColor: "#1ce6e9",
    width: 25,
    height: 25,
    marginTop: Platform.OS === "ios" ? 50 : 20,
    marginLeft: 10,
    transform: [{ scaleX: I18nManager.isRTL ? -1 : 1 }],
  },
};

const customFonts = {
  h1: {
    fontFamily: fontFamily.black,
    fontSize: fontSet.xxlarge,
  },
  h2: {
    fontFamily: fontFamily.bold,
    fontSize: fontSet.xlarge,
  },
  h3: {
    fontFamily: fontFamily.bold,
    fontSize: fontSet.large,
  },
  p1: {
    fontFamily: fontFamily.medium,
    fontSize: fontSet.middle,
  },
  p2: {
    fontFamily: fontFamily.medium,
    fontSize: fontSet.normal,
  },
  p3: {
    fontFamily: fontFamily.main,
    fontSize: fontSet.small,
  },
};

const genericInput = {
  InputContainer: {
    height: 42,
    borderWidth: 1,
    borderColor: colorSet.grey3,
    backgroundColor: colorSet.grey0,
    //borderColor: "rgba(12, 204, 187, 1)",
    /*backgroundColor: modedColor(
      appStyles.colorSet[colorScheme].mainThemeBackgroundColor,
      TNColor('#e0e0e0'),
    ),*/
    paddingLeft: 10,
    color: colorSet.grey9,
    width: "90%",
    alignSelf: "center",
    marginTop: 10,
    alignItems: "center",
    borderRadius: 25,
    flex: 1,
    top: "1%",
    marginBottom: 15,
    fontSize: 15,
  },
  labelInput: {
    alignSelf: "center",
    color: "rgba(1, 90, 83, 1)",
    fontSize: 18,
    fontWeight: "800",
    flex: 1,
    top: "1%",
    width: "90%",
    paddingLeft: "2%",
    marginBottom: 0,
  },
};

const StyleDict = {
  fontFamily,
  colorSet,
  navThemeConstants,
  fontSet,
  sizeSet,
  iconSet,
  styleSet,
  WINDOW_WIDTH,
  WINDOW_HEIGHT,
  customFonts,
  genericInput,
};

export default StyleDict;
