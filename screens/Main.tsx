import React from "react";
import { Button, View } from "react-native";

export default function Main(props) {
  const navigation = props.navigation;

  const onSendMessageButtonPress = () => {
    const driverID = "myId"; // driver - me
    const viewerID = "id"; // user
    const userData = {
      id: "id",
      firstName: "usuario",
      lastName: "prueba",
      profilePictureURL:
        "https://firebasestorage.googleapis.com/v0/b/zippi-ejc-development.appspot.com/o/avatar%2FXAEwrDlvk2TEs6RemYOTa9hraZZ2%2Fb06c046f-a376-4cca-aa1b-c349afdfca58.jpg?alt=media&token=b3f519de-c39d-4e8b-a5d5-cced22c52472",
    };

    let channel = {
      id: viewerID < driverID ? viewerID + driverID : driverID + viewerID,
      participants: [userData],
    };

    navigation.navigate("PersonalChat", {
      channel,
      appStyles: {},
    });
  };

  return (
    <View style={{ flex: 1, justifyContent: "center" }}>
      <Button
        onPress={() => {
          onSendMessageButtonPress();
        }}
        title="Test"
      ></Button>
    </View>
  );
}
