import { StatusBar } from "expo-status-bar";
import React from "react";
import { StyleSheet } from "react-native";
import { AppearanceProvider } from "react-native-appearance";
import "react-native-gesture-handler";
import { Provider } from "react-redux";
import Main from "./navigation";
import useStartFirebase from "./src/firebase/use-start-firebase";
import reduxStore from "./src/redux/store";

export default function App() {
  useStartFirebase();
  return (
    <Provider store={reduxStore}>
      <AppearanceProvider>
        <StatusBar style="auto" />
        <Main />
      </AppearanceProvider>
    </Provider>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white",
    // alignItems: "center",
    // justifyContent: "center",
  },
});
