import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import React from "react";
import ContainerScreen from "../screens/ContainerScreen";
import MainScreen from "../screens/Main";
const Stack = createStackNavigator();

export default function Main() {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Main">
        <Stack.Screen
          name="Main"
          options={{ headerShown: false }}
          component={MainScreen}
        />
        <Stack.Screen
          name="PersonalChat"
          // options={{ headerShown: false }}
          component={ContainerScreen}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
}
