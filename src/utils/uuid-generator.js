import uuid from "react-native-uuid";
uuid.v4();

export default function generateUuid() {
  return uuid.v4();
}
