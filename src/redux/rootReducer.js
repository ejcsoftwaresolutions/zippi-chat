import { combineReducers } from "redux";
import chatReducer from "./reducers/chat";

// different slices of state
const rootReducer = combineReducers({
  chat: chatReducer,
});

export default rootReducer;
