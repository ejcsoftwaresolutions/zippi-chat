import rootReducer from './rootReducer';
import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';

const reduxStore = createStore(rootReducer, applyMiddleware(thunk));

export default reduxStore;