import { useSelector, useDispatch } from 'react-redux';

const useStore = () => {
  const state = useSelector((state) => state);
  const dispatch = useDispatch();
  return [state, dispatch];
};

export default useStore;