import { SET_CHANNELS, SET_CHANNELS_SUBCRIBED } from "../actions/actionTypes";

const initialState = {
  channels: null,
  areChannelsSubcribed: false,
  reload: false,
};

const chatReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_CHANNELS_SUBCRIBED:
      return {
        ...state,
        areChannelsSubcribed: action.payload,
      };
    case SET_CHANNELS:
      return {
        ...state,
        channels: [...action.payload],
      };
    case "LOG_OUT":
      return initialState;
    default:
      return state;
  }
};

export default chatReducer;
