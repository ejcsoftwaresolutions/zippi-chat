import { SET_CHANNELS, SET_CHANNELS_SUBCRIBED } from './actionTypes';

export const setChannels = (data) => {
  return (dispatch) => {
    dispatch(setChannelsFn(data));
  };
};

export const setChannelsFn = (data) => ({
  type: SET_CHANNELS,
  payload: data,
});

////////////////////////////////////////////////////

export const setChannelsSubcribed = (data) => {
  return (dispatch) => {
    dispatch(setChannelsSubcribedFn(data));
  };
};

export const setChannelsSubcribedFn = (data) => ({
  type: SET_CHANNELS_SUBCRIBED,
  payload: data,
});

////////////////////////////////////////////////////
