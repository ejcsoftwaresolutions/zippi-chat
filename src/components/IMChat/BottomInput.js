import { IMLocalized } from "@utils";
import * as Permissions from "expo-permissions";
import React, { useEffect, useRef, useState } from "react";
import {
  Alert,
  Image,
  Text,
  TouchableHighlight,
  TouchableOpacity,
  View,
} from "react-native";
import { useColorScheme } from "react-native-appearance";
import { AutoGrowingTextInput } from "react-native-autogrow-textinput";
import { KeyboardAccessoryView } from "react-native-keyboard-accessory";
//import { KeyboardAccessoryView } from "react-native-ui-lib/keyboard";
import BottomAudioRecorder from "./BottomAudioRecorder";
import dynamicStyles from "./styles";

const assets = {
  cameraFilled: require("@assets/camera-filled.png"),
  send: require("@assets/send.png"),
  mic: require("@assets/microphone.png"),
  close: require("@assets/close-x-icon.png"),
};

function BottomInput(props) {
  const {
    value,
    onChangeText,
    onAudioRecordDone,
    onSend,
    onAddMediaPress,
    uploadProgress,
    appStyles,
    trackInteractive,
    inReplyToItem,
    onReplyingToDismiss,
  } = props;

  const colorScheme = useColorScheme();
  const styles = dynamicStyles(appStyles, colorScheme);
  const textInputRef = useRef(null);
  const [customKeyboard, setCustomKeyboard] = useState({
    component: undefined,
    initialProps: undefined,
  });

  useEffect(() => {
    // textInputRef.current?.focus();
  }, []);

  const isDisabled = () => {
    if (/\S/.test(value)) {
      return false;
    } else {
      return true;
    }
  };

  const onKeyboardResigned = () => {
    resetKeyboardView();
  };

  const resetKeyboardView = () => {
    setCustomKeyboard({});
  };

  const onVoiceRecord = async () => {
    if (customKeyboard.component == "BottomAudioRecorder") {
      setCustomKeyboard({
        component: undefined,
        initialProps: undefined,
      });
      return;
    }

    const response = await Permissions.askAsync(Permissions.AUDIO_RECORDING);

    if (response.status === "granted") {
      showKeyboardView("BottomAudioRecorder");
    } else {
      Alert.alert(
        IMLocalized("Audio permission denied"),
        IMLocalized(
          "You must enable audio recording permissions in order to send a voice note."
        )
      );
    }
  };

  const showKeyboardView = (component) => {
    setCustomKeyboard({
      component,
      initialProps: { appStyles },
    });
  };

  const onCustomKeyboardItemSelected = (keyboardId, params) => {
    onAudioRecordDone(params);
  };

  const renderBottomInput = () => {
    return (
      <View style={styles.bottomContentContainer}>
        {inReplyToItem && (
          <View style={styles.inReplyToView}>
            <Text style={styles.replyingToHeaderText}>
              {IMLocalized("Replying to")}{" "}
              <Text style={styles.replyingToNameText}>
                {inReplyToItem.senderFirstName || inReplyToItem.senderLastName}
              </Text>
            </Text>
            <Text style={styles.replyingToContentText}>
              {inReplyToItem.content}
            </Text>
            <TouchableHighlight
              style={styles.replyingToCloseButton}
              onPress={() => onReplyingToDismiss && onReplyingToDismiss()}
            >
              <Image source={assets.close} style={styles.replyingToCloseIcon} />
            </TouchableHighlight>
          </View>
        )}
        <View style={[styles.progressBar, { width: `${uploadProgress}%` }]} />
        <View style={styles.inputBar}>
          <TouchableOpacity
            onPress={onAddMediaPress}
            style={styles.inputIconContainer}
          >
            <Image style={styles.inputIcon} source={assets.cameraFilled} />
          </TouchableOpacity>
          <View style={styles.inputContainer}>
            <TouchableOpacity
              onPress={onVoiceRecord}
              style={styles.micIconContainer}
            >
              <Image style={styles.micIcon} source={assets.mic} />
            </TouchableOpacity>
            <AutoGrowingTextInput
              maxHeight={100}
              style={styles.input}
              ref={textInputRef}
              value={value}
              multiline={true}
              placeholder={IMLocalized("Start typing...")}
              placeholderTextColor={
                appStyles.colorSet[colorScheme].mainSubtextColor
              }
              underlineColorAndroid="transparent"
              onChangeText={(text) => onChangeText(text)}
              onFocus={resetKeyboardView}
            />
          </View>
          <TouchableOpacity
            disabled={isDisabled()}
            onPress={onSend}
            style={[
              styles.inputIconContainer,
              isDisabled() ? { opacity: 0.2 } : { opacity: 1 },
            ]}
          >
            <Image style={styles.inputIcon} source={assets.send} />
          </TouchableOpacity>
        </View>
      </View>
    );
  };
  return (
    <KeyboardAccessoryView alwaysVisible={true} androidAdjustResize>
      <View>{renderBottomInput()}</View>
      {customKeyboard && customKeyboard.component == "BottomAudioRecorder" ? (
        <View style={{ height: 200 }}>
          <BottomAudioRecorder
            appStyles={appStyles}
            onRecordSend={(params) => {
              setCustomKeyboard({
                component: undefined,
                initialProps: undefined,
              });
              onAudioRecordDone(params);
            }}
          />
        </View>
      ) : (
        <View />
      )}
    </KeyboardAccessoryView>
  );
  // return (
  //   <KeyboardAccessoryView
  //     renderContent={renderBottomInput}
  //     trackInteractive={trackInteractive}
  //     kbInputRef={textInputRef}
  //     kbComponent={customKeyboard.component}
  //     kbInitialProps={customKeyboard.initialProps}
  //     onItemSelected={onCustomKeyboardItemSelected}
  //     onKeyboardResigned={onKeyboardResigned}
  //     revealKeyboardInteractive
  //   />
  // );
}

export default BottomInput;
